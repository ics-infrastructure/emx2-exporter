import re
import logging
from urllib.parse import parse_qs
from emx_scraper_snmp import snmp_scrape


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("Emx-App")

CONTENT_TYPE = ("Content-Type", "text/plain")


def metrics(environ, start_response):
    parameters = parse_qs(environ.get("QUERY_STRING", ""))
    if "target" not in parameters.keys():
        start_response("417 EXPECTATION FAILED", [CONTENT_TYPE])
        return [bytes("'target' parameter missing", "utf-8")]
    logger.debug("Parameters: %s", parameters)

    logger.info(
        "Stating scrape, target=%s client=%s",
        parameters["target"][0],
        environ["REMOTE_ADDR"],
    )

    try:
        data = ""
        data = snmp_scrape(host=parameters["target"][0])
    except Exception as error:
        logger.exception(error)
        start_response("503 SERVICE UNAVAILABLE", [CONTENT_TYPE])
        return [bytes(str(error), "utf-8")]
    status = "200 OK"
    response_headers = [
        ("Content-type", "text/plain"),
        ("Content-Length", str(len(data))),
    ]
    start_response(status, response_headers)
    return [bytes(data, "utf-8")]


urls = [(r"metrics/?$", metrics), (r"metrics/(.+)$", metrics)]


def not_found(environ, start_response):
    """Called if no URL matches."""
    start_response("404 NOT FOUND", [CONTENT_TYPE])
    return [bytes("Not Found", "utf-8")]


def app(environ, start_response):
    """The main WSGI application.

    Dispatch the current request to
    the functions from above and store the regular expression
    captures in the WSGI environment as  `myapp.url_args` so that
    the functions from above can access the url placeholders.

    If nothing matches call the `not_found` function.
    """
    path = environ.get("PATH_INFO", "").lstrip("/")
    for regex, callback in urls:
        match = re.search(regex, path)
        if match is not None:
            environ["app.url_args"] = match.groups()
            return callback(environ, start_response)
    return not_found(environ, start_response)
