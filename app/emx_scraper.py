import logging
from paramiko import SSHClient, AutoAddPolicy
from metrics import Metrics


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("EmxSSH")
logging.getLogger("paramiko.transport").setLevel(logging.CRITICAL)


class EmxSSH:
    def __init__(self, target, username, key_filename, namespace="emx"):
        self.client = SSHClient()
        self.client.set_missing_host_key_policy(AutoAddPolicy())
        self._sshconfig = {
            "hostname": target,
            "username": username,
            "key_filename": key_filename,
            "timeout": 10,
            "banner_timeout": 5,
            "auth_timeout": 5,
        }
        self.registry = Metrics(namespace=namespace)
        logging.debug("Init for %s", target)

    def run(self):
        logging.debug("Running for %s", self._sshconfig["hostname"])
        self.client.connect(**self._sshconfig)
        logging.debug("Connected to %s", self._sshconfig["hostname"])
        self.parse_emd_details()
        self.parse_externalsensors()
        self.client.close()
        logging.debug("Rendering parsed metrics for %s", self._sshconfig["hostname"])
        return self.registry.collect()

    def parse_externalsensors(self):
        command_out, _ = self._run_command("show externalsensors")
        sensors = []
        lines_tmp = []
        for line in list(filter(self.is_not_cli_garbage, command_out)):
            if not line == "":
                lines_tmp.append(line)
            else:
                sensors.append(list(lines_tmp))
                lines_tmp = []
        self.registry.register("sensor_state", "gauge")
        for sensor in list(filter(lambda x: (x != []), sensors)):
            self.parse_sensor(sensor)
        logging.debug("External sensors parsed for %s", self._sshconfig["hostname"])

    def parse_sensor(self, sensor_raw):
        def _sanitize_label(label):
            return (
                label.replace(" ", "_")
                .replace("(", "")
                .replace(")", "")
                .replace("/", " ")
            )

        sensor = {}
        for line in sensor_raw:
            splitted = line.split(":")
            if len(splitted) < 2:
                sensor["sensor_name"] = line.split("'")[1]
            else:
                if splitted[0] == "Sensor type":
                    sensor["sensor_type"] = _sanitize_label(splitted[1].strip())
                if splitted[0] == "Channel":
                    sensor["sensor_channel"] = splitted[1].strip()
                if splitted[0] == "State":
                    sensor["value"] = 0 if splitted[1].strip() == "Normal" else 1
        if "sensor_name" in sensor.keys() and "value" in sensor.keys():
            self.registry.add_metric(
                "sensor_state",
                sensor["value"],
                labels={
                    "sensor_channel": sensor["sensor_channel"],
                    "sensor_type": sensor["sensor_type"],
                },
            )

    def _run_command(self, command):
        ssh_stdin, ssh_stdout, ssh_stderr = self.client.exec_command(
            "", timeout=self._sshconfig["timeout"]
        )
        ssh_stdin.write(f"\n{command}")
        ssh_stdin.channel.shutdown_write()
        std_out = ssh_stdout.read().decode("utf8").split("\n")
        std_err = ssh_stderr.read().decode("utf8").split("\n")
        ssh_stdin.close()
        ssh_stdout.close()
        ssh_stderr.close()
        return std_out, std_err

    def is_not_cli_garbage(self, line):
        return not (
            # line == ""
            line.startswith("#")
            or line == "Welcome to EMX CLI!"
            or ("Last login:" in line and "[CLI (SSH) from " in line)
        )

    def parse_emd_details(self):
        command_out, _ = self._run_command("show emd details")
        for line in filter(self.is_not_cli_garbage, command_out):
            splitted = line.split(":")
            if len(splitted) == 2:
                name = splitted[0].strip().lower().replace(" ", "_")
                value = splitted[1].strip()
                if name in [
                    "model",
                    "firmware_version",
                    "serial_number",
                    "board_revision",
                    "hardware_id",
                ]:
                    self.registry.add_common_label(name, value)
                    continue
                if name == "internal_beeper":
                    self.registry.register(name, "gauge")
                    self.registry.add_metric(name, 1 if value == "On" else 0)
                    continue
                if name == "measurements_per_log_entry":
                    self.registry.register(name, "gauge")
                    self.registry.add_metric(name, value)
                    continue
                if name in [
                    "peripheral_device_auto_management",
                    "sensor_data_retrieval",
                ]:
                    self.registry.register(name, "gauge")
                    self.registry.add_metric(name, 1 if value == "Enabled" else 0)
                    continue
                if name == "device_altitude":
                    self.registry.register("device_altitude_meters", "gauge")
                    self.registry.add_metric(
                        "device_altitude_meters", value.split(" ")[0]
                    )
                    continue
                if name == "external_sensor_z_coordinate_format":
                    continue  # defaults to "Rack units"
            if len(splitted) == 1:
                if line.startswith("EMD '"):
                    self.registry.add_common_label("device_name", line.split("'")[1])
                    continue
        self.registry.register("up", "gauge")
        self.registry.add_metric("up", "1")
        logging.debug("Emd details parsed for %s", self._sshconfig["hostname"])
