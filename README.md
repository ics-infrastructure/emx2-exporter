# emx2 exporter

This exporter uses the ssh interface of the emx2-888 to gather statistics and present it in a prometheus compatible format.

## How to run

To run the application see the provided docker-compose.yml file.


## License

BSD 2-clause
