import logging
from easysnmp import Session
from metrics import Metrics

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("Emx")


def snmp_scrape(host, metrics_namespace="shx", snmp_community="public"):
    metrics = []

    def _sanitize_label(label):
        return (
            label.replace(" ", "_")
            .replace("(", "")
            .replace(")", "")
            .replace("/", " ")
            .lower()
        )

    session = Session(hostname=host, community=snmp_community, version=2)
    device_items = session.walk("iso.3.6.1.4.1.13742.9.1.3.1.1.7")
    logger.debug("Devices found: %s", device_items)

    for device_item in device_items:
        device_model = device_item.value
        device_id = device_item.oid_index
        logger.debug("Parsing: %s - %s", device_id, device_model)

        device_sensors_items = session.walk(
            f"iso.3.6.1.4.1.13742.9.1.4.1.1.17.{device_id}"
        )
        for sensor_item in device_sensors_items:
            sensor_name = _sanitize_label(sensor_item.value)
            sensor_id = sensor_item.oid_index

            sensor_value_item = session.get(
                f"iso.3.6.1.4.1.13742.9.2.1.1.3.{sensor_id}"
            )
            if sensor_value_item.value != "NOSUCHINSTANCE":
                sensor_value = sensor_value_item.value
            else:
                sensor_value = -127
                logger.error('Cannot parse sensor value %s', sensor_value_item)

            sensor_state_item = session.get(
                f"iso.3.6.1.4.1.13742.9.2.1.1.2.{sensor_id}"
            )
            if sensor_state_item.value != 'NOSUCHINSTANCE':
                sensor_state = sensor_state_item.value
            else:
                sensor_state = -1

            labels = {
                "device_model": device_model,
                "device_id": device_id,
                "sensor_name": sensor_name,
                "sensor_id": sensor_id
            }

            if sensor_name.startswith("temperature_"):
                sensor_value = float(int(sensor_value) / 10)
                metric_name = f"{metrics_namespace}_temperature"
            elif sensor_name.startswith("fan_speed_"):
                metric_name = f"{metrics_namespace}_rpm"
            elif sensor_name.startswith("valve_position"):
                metric_name = f"{metrics_namespace}_valve_position"
                sensor_value = float(int(sensor_value) / 100)
            else:
                # FIXME: log!
                continue
            logger.debug("adding to metrics %s", (metric_name,
                                                  sensor_value, labels))
            metrics.append((metric_name, sensor_value, labels))
            metrics.append((f"{metrics_namespace}_state_raw",
                            sensor_state, labels))

    metrics_name = list(dict.fromkeys([x[0] for x in metrics]))
    registry = Metrics(namespace='emx')
    for metric in metrics_name:
        registry.register(metric, "gauge")
    for metric in metrics:
        registry.add_metric(metric[0], metric[1], metric[2])
    return(registry.collect())


if __name__ == "__main__":
    print(snmp_scrape(host="172.17.38.14"))
